
# Deploy Application from Jenkins to EC2 with Docker Compose
CD - Deploy Application from Jenkins Pipeline on EC2
Instance (automatically with docker-compose)

## Technologies Used
- AWS
- Jenkins
- Docker
- Linux
- Git
- Java
- Maven
- Docker Hub

## Project Description
- Install Docker Compose on AWS EC2 Instance
- Create docker-compose.yml file that deploys our web
application image
- Configure Jenkins pipeline to deploy newly built image using Docker Compose on EC2 server
- Improvement: Extract multiple Linux commands that are executed on remote server into a separate shell script and execute the script from Jenkinsfile

## Prerequisites
- An AWS account
- Docker account
- Linux machine configured with Docker and Git
- Private Jenkins server (from previous courses)

## Guide Steps
### Install Docker Compose
- SSH into your EC2 instance as admin
- Download Docker-Compose
	- `sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose`
- Add execute permission on the docker-compose file
	- `sudo chmod +x /usr/local/bin/docker-compose`
- Verify Install
	- `docker-compose --version`

### Creating a docker-compose.yaml
- We will add our image from our private Docker Repository and the ports we previously had set in the `docker run` command on project m9-2.
```yaml
java-maven-app:
	image: DOCKER_HUB/APP_NAME:VERSION
	ports:
		- "8080:8080"
```

- We also want to add a Postgres database container by using this code snippet
```yaml
postgres:
	image: postgres:15
	ports:
		- "5432:5432"
	environment:
		- POSTGRES_PASSWORD=my-pwd
```

### Modify our Jenkinsfile
- We will edit our `deploy` stage to utilize a docker-compose command instead of a docker run one.

**Old Code**
```groovy
 def dockerCmd = "docker run -d -p 8080:8080 ${IMAGE_NAME}"
 sshagent(['ec2-server-key']) {
 sh "ssh -o StrictHostKeyChecking=no ec2-user@3.95.62.40 ${dockerCmd}"
```

**Modified Code**
```groovy
 def dockerComposeCmd = "docker-compose -f docker-compose.yaml up --detach"
 sshagent(['ec2-server-key']) {
 sh "scp docker-compose.yaml ec2-user@EC2_IP:/home/ec2-user"
 sh "ssh -o StrictHostKeyChecking=no ec2-user@3.95.62.40 ${dockerComposeCmd}"
```
- Commit the changes and run the build from Jenkins, the application will now be running on our EC2 instance and accessible via the web browser.

![Successful Web App Launch](/images/m9-3-successful-web-app-launch.png)

![Containers Running](/images/m9-3-containers-running.png)

### Improvement | Extract to a Shell Script
- We will add a new file called `shell-cmds.sh` to our project and extract the code from our previous. This will give us flexibility to run multiple commands.

```bash
#shell-cms.sh
docker-compose -f docker-compose.yaml up --detach
echo "Success"
```
**Jenkinsfile**
```groovy
 def shellCmd = "bash ./server-cmds.sh"
 sshagent(['ec2-server-key']) {
 sh "scp server-cmds.sh ec2-user@EC2_IP:/home/ec2-user"
 sh "scp docker-compose.yaml ec2-user@EC2_IP:/home/ec2-user"
 sh "ssh -o StrictHostKeyChecking=no ec2-user@3.95.62.40 ${shellCmd }"
```

![Shell Script Works](/images/m9-3-shell-script-works.png)

### Improvement | Replace Docker Image with newly built version
- Modify the docker-compose.yaml to use a variable for our image name instead of a hardcoded value
```yaml
image: ${IMAGE}
```

- Modify our Jenkinsfile to send our shell script a parameter for the IMAGE_NAME
```groovy
def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}
```

- We will use environment variables on the EC2 instance for our docker-compose command to reference for our image.

```bash
export IMAGE=$1
```
